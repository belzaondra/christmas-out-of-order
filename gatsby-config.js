module.exports = {
  siteMetadata: {
    siteUrl: "https://www.yourdomain.tld",
    title: "christmas out of order",
  },
  plugins: [
    "gatsby-plugin-react-helmet",
    {
      resolve: "gatsby-plugin-manifest",
      options: {
        icon: "src/images/icon.png",
      },
    },
  ],
};
