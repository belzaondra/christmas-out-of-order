import * as React from "react";
import { Helmet } from "react-helmet";
import "../styles/global.css";
// markup

const IndexPage = () => {
  return (
    <main>
      <Helmet>
        <title>Christmas app</title>
      </Helmet>
      <div
        style={{
          height: "100%",
          minHeight: "100vh",
          justifyContent: "center",
          alignItems: "center",
          display: "flex",
          flexDirection: "column",
          padding: "1rem",
        }}>
        <h1
          style={{
            color: "#8D99AE",
            display: "inline-block",
            fontSize: "5rem",
            textAlign: "center",
          }}>
          Aplikace bude pokračovat o Vánocích 2022
          <br />
          <span style={{ textAlign: "center", display: "block" }}>👋</span>
        </h1>
        <a
          href="https://www.linkedin.com/in/ond%C5%99ej-belza-b8484a187/"
          style={{ textDecoration: "none", color: "#8D99AE" }}>
          Ondrej Belza
        </a>
      </div>
    </main>
  );
};

export default IndexPage;
